package hasher

import (
	"crypto/sha256"
	"errors"
	"fmt"
)

func HashText(text string, rounds int) (string, error) {
	if rounds <= 0 {
		return "", ErrBadRounds
	}

	h := sha256.New()

	// First hashing round
	h.Write([]byte(text))

	// Additional hashing rounds (if necessary)
	for i := 1; i < rounds; i++ {
		prev_round := h.Sum(nil)
		h.Reset()
		h.Write(prev_round)
	}

	hash := fmt.Sprintf("%x", h.Sum(nil))
	return hash, nil
}

var ErrBadRounds = errors.New("rounds must be positive integer value")
