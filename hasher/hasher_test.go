package hasher

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHasher(t *testing.T) {
	hash, err := HashText("the answer is 42", 0)
	assert.Error(t, err)

	hash, err = HashText("the answer is 42", 1)
	assert.NoError(t, err)
	assert.Equal(t, "ff29438fb7a23c7eb348c56013db4df7f44bf5b081c3430c76913ebcacba6b70", hash)

	hash, err = HashText("the answer is 42", 2)
	assert.NoError(t, err)
	assert.Equal(t, "9b88c4e93df03337a469e28e65a28b5b9f6bcc9c333f62d50cf6cff87ab17fcd", hash)

	hash, err = HashText("the answer is 42", 3)
	assert.NoError(t, err)
	assert.Equal(t, "86e11c581362b1daab824f28a571ef05d67cbdf6816d2b2716c284c4623babb0", hash)
}
