package dbpostgres

import (
	"database/sql"
	_ "github.com/lib/pq"
	"hashservice/dbapi"
)

type PostgresDatabase struct {
	db *sql.DB
}

func Connect(dataSource string) (dbapi.HashTaskDatabase, error) {
	db, err := sql.Open("postgres", dataSource)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		_ = db.Close()
		return nil, err
	}

	if err := initialize(db); err != nil {
		_ = db.Close()
		return nil, err
	}

	return &PostgresDatabase{db: db}, nil
}

func initialize(db *sql.DB) error {
	query := `
CREATE TABLE IF NOT EXISTS tasks (
	task_id SERIAL NOT NULL,
	payload TEXT NOT NULL,
	rounds INT NOT NULL,
	hash VARCHAR(64) NULL,
	PRIMARY KEY (task_id)
)`

	_, err := db.Exec(query)
	if err != nil {
		return err
	}

	return nil
}

func (db *PostgresDatabase) Close() error {
	return db.db.Close()
}

func (db *PostgresDatabase) CreateTask(text string, rounds int) (*dbapi.HashTask, error) {
	query := `INSERT INTO tasks (payload, rounds, hash) VALUES ($1, $2, NULL) RETURNING task_id`

	var id int64
	err := db.db.QueryRow(query, text, rounds).Scan(&id)
	if err != nil {
		return nil, err
	}

	task := dbapi.HashTask{
		Id:     dbapi.TaskId(id),
		Text:   text,
		Rounds: rounds,
		Status: dbapi.InProgress,
		Hash:   nil,
	}

	return &task, nil
}

func (db *PostgresDatabase) FinishTask(id dbapi.TaskId, hash string) error {
	query := `UPDATE tasks SET hash = $2 WHERE task_id = $1`

	res, err := db.db.Exec(query, id, hash)
	if err != nil {
		return err
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if rows == 0 {
		return dbapi.ErrTaskNotFound
	}

	return nil
}

func (db *PostgresDatabase) FindTask(id dbapi.TaskId) (*dbapi.HashTask, error) {
	var task dbapi.HashTask
	task.Id = id

	query := `SELECT payload, rounds, hash FROM tasks WHERE task_id = $1`
	err := db.db.QueryRow(query, id).Scan(&task.Text, &task.Rounds, &task.Hash)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, dbapi.ErrTaskNotFound
		}
		return nil, err
	}

	if task.Hash == nil {
		task.Status = dbapi.InProgress
	} else {
		task.Status = dbapi.Finished
	}

	return &task, nil
}

func (db *PostgresDatabase) LoadUnfinishedTasks() ([]dbapi.HashTask, error) {
	query := `SELECT task_id, payload, rounds FROM tasks WHERE hash IS NULL`
	rows, err := db.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer func() { _ = rows.Close() }()

	tasks := make([]dbapi.HashTask, 0)

	for rows.Next() {
		if err := rows.Err(); err != nil {
			return nil, err
		}

		var task dbapi.HashTask
		err = rows.Scan(&task.Id, &task.Text, &task.Rounds)
		if err != nil {
			return nil, err
		}
		task.Status = dbapi.InProgress

		tasks = append(tasks, task)
	}

	return tasks, nil
}
