Hash REST API web service
=========================

A simple hashing web service implemented in Golang.


Build
-----

Clone from git repository and execute `go build`.


Running without a database (for debugging purposes)
---------------------------------------------------

The service can be run without an SQL database for simplicity purposes.

```
$ hashservice --no-database
```

In this case service will run with in-memory storage which is lost when program exited.


Running with PostgreSQL database
--------------------------------

To enable data persistence run service configured as follows:

```
$ hashservice --postgres --datasource "dbname=hashdb user=postgres password=123456 sslmode=disable"
```

The specified database (`hashdb` in this example) must be created manually prior to running the service.
The necessary tables will be created automatically on first run, so no need to create any tables manually.


HTTP server
-----------

The service is run on port `8080` by default. This can be overridden by specifying command line argument `--port 8081`.


RESTful API
-----------

All resources are returned in JSON format.

Some endpoints allow `GET` method, while others require `POST`. See below for detailed description.

##### http://localhost:8080

Root document on the server returns a brief description of available API base paths:

```json
{"link": "/hash-api/v1", "method": "GET", "descr": "Hash API, version 1.0"}
```


##### http://localhost:8080/hash-api/v1

Base path of the REST API is `/user-api/v1`. Document at that location describes available resources under this API.

```json
[
  {"link": "/hash-api/v1/tasks", "method": "POST", "descr": "Post a new text to hash"},
  {"link": "/hash-api/v1/tasks/{task-id}", "method": "GET", "descr": "Get task status and hashing result"}
]
```


##### http://localhost:8080/hash-api/v1/tasks

This endpoint requires `POST` method to create a new task.

POST data:
```json
{"payload": "the answer is 42", "hash_rounds_cnt": 3}
```

Response:
```json
{"id":1, "payload": "the answer is 42", "hash_rounds_cnt": 3, "status": "in progress", "hash": null, "link": "/hash-api/v1/tasks/1"}
```

The returned JSON document contains a link to resource where result could be requested.


##### http://localhost:8080/hash-api/v1/tasks/{id}

This location returns hash operation result by id if `GET` method used.

Response for `GET http://localhost:8080/hash-api/v1/tasks/1`:
```json
{
  "id": 1,
  "payload": "the answer is 42",
  "hash_rounds_cnt": 3,
  "status": "finished",
  "hash": "86e11c581362b1daab824f28a571ef05d67cbdf6816d2b2716c284c4623babb0",
  "link": "/hash-api/v1/tasks/1"
}
```


Source code
-----------

Service is written as a go module and requires go version 1.11 or higher to build.

File `main.go` is the application entry point.
It parses command line arguments and launches the service with appropriate database.

File `server/server.go` is the HTTP server stuff: routing, handlers etc.

File `server/api.go` is JSON model for the service.

Package `dbapi` is the abstraction layer for the database.
It is implemented as in-memory database and SQL database.

Package `dbinmem` is the in-memory database implementation of the API defined in `dbapi/db.go`.

Package `dbpostgres` is the PostgreSQL database implementation of the API defined in `dbapi/db.go`.

Package `hasher` is the implementation of multi-round hashing using SHA-256 algorithm.


Further improvements
--------------------

Currently every new hash request is computed unconditionally. Though the requested combination of payload and number
of hashing rounds could already be computed before, so we can look up the database for it, and if matching record found,
just return ID of the already computed task.

Current implementation assumes that database connection is stable and no reconnection attempt is made if connection is lost.
For production-ready service this should be fixed.

This implementation is configured from the command line arguments. It could be sometimes better to have a separate
config file for that.