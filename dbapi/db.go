package dbapi

import "errors"

type HashTaskDatabase interface {
	Close() error
	CreateTask(text string, rounds int) (*HashTask, error)
	FinishTask(id TaskId, hash string) error
	FindTask(id TaskId) (*HashTask, error)
	LoadUnfinishedTasks() ([]HashTask, error)
}

type HashTask struct {
	Id     TaskId
	Text   string
	Rounds int
	Status TaskStatus
	Hash   *string
}

type TaskId int64

type TaskStatus int

const (
	_ TaskStatus = iota
	InProgress
	Finished
)

var ErrTaskNotFound = errors.New("task not found")
