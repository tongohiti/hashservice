package server

import (
	"fmt"
	"hashservice/dbapi"
)

type HashApiHelp struct {
	Link        string `json:"link"`
	Method      string `json:"method"`
	Description string `json:"descr"`
}

type NewTaskInput struct {
	Text   string `json:"payload"`
	Rounds int    `json:"hash_rounds_cnt"`
}

type TaskOutput struct {
	Id     int     `json:"id"`
	Text   string  `json:"payload"`
	Rounds int     `json:"hash_rounds_cnt"`
	Status string  `json:"status"`
	Hash   *string `json:"hash"`
	Link   string  `json:"link"`
}

const (
	StatusInProgress = "in progress"
	StatusFinished   = "finished"
)

func convertTaskToJsonModel(task *dbapi.HashTask, uriBase string) TaskOutput {
	return TaskOutput{
		Id:     int(task.Id),
		Text:   task.Text,
		Rounds: task.Rounds,
		Status: convertStatusToJsonModel(task.Status),
		Hash:   task.Hash,
		Link:   fmt.Sprintf("%s%d", uriBase, task.Id),
	}
}

func convertStatusToJsonModel(status dbapi.TaskStatus) string {
	switch status {
	case dbapi.InProgress:
		return StatusInProgress
	case dbapi.Finished:
		return StatusFinished
	default:
		return ""
	}
}
