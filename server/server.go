package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"hashservice/dbapi"
	"log"
	"net/http"
	"path"
	"strconv"
	"strings"
)

const (
	Root           = "/"
	UriBase        = "/hash-api/v1"
	UriPostNewTask = "/hash-api/v1/tasks"
	UriGetResult   = "/hash-api/v1/tasks/"
)

type hashApiHandler struct {
	database    dbapi.HashTaskDatabase
	computeFunc ComputeFunc
}

type ComputeFunc func(dbapi.HashTask, dbapi.HashTaskDatabase)

func Run(hostPort string, db dbapi.HashTaskDatabase, computeFunc ComputeFunc) {
	handler := hashApiHandler{database: db, computeFunc: computeFunc}

	http.HandleFunc(Root, serverRootHandler)
	http.HandleFunc(UriBase, hashApiRootHandler)
	http.Handle(UriPostNewTask, &handler)
	http.Handle(UriGetResult, &handler)

	log.Printf("Listening on %s", hostPort)
	log.Fatal(http.ListenAndServe(hostPort, nil))
}

func serverRootHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != Root {
		http.NotFound(w, r)
		return
	}

	if r.Method != http.MethodGet {
		http.Error(w, "405 Method not allowed, use GET", http.StatusMethodNotAllowed)
		return
	}

	resp := HashApiHelp{
		Link:        UriBase,
		Method:      http.MethodGet,
		Description: "Hash API, version 1.0",
	}

	respondJson(w, r, resp, http.StatusOK)
}

func hashApiRootHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "405 Method not allowed, use GET", http.StatusMethodNotAllowed)
		return
	}

	resp := [...]HashApiHelp{
		{
			Link:        UriPostNewTask,
			Method:      http.MethodPost,
			Description: "Post a new text to hash",
		},
		{
			Link:        UriGetResult + "{task-id}",
			Method:      http.MethodGet,
			Description: "Get task status and hashing result",
		},
	}

	respondJson(w, r, resp, http.StatusOK)
}

func (h *hashApiHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch urlPath := r.URL.Path; {
	case urlPath == UriPostNewTask:
		postNewTaskHandler(w, r, h.database, h.computeFunc)

	case strings.HasPrefix(urlPath, UriGetResult):
		getResultHandler(w, r, h.database)

	default:
		http.Error(w, fmt.Sprintf("404 Not found: %s", urlPath), http.StatusNotFound)
	}
}

func postNewTaskHandler(w http.ResponseWriter, r *http.Request, database dbapi.HashTaskDatabase, computeFunc ComputeFunc) {
	if r.Method != http.MethodPost {
		http.Error(w, "405 Method not allowed, use POST", http.StatusMethodNotAllowed)
		return
	}

	var input NewTaskInput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		respondBadRequest(w, r, err)
		return
	}
	if input.Rounds <= 0 {
		respondBadRequest(w, r, errors.New("invalid number of rounds"))
		return
	}

	log.Printf("New hash request: `%s` x%d", input.Text, input.Rounds)

	task, err := database.CreateTask(input.Text, input.Rounds)
	if err != nil {
		respondServerError(w, r, err)
		return
	}

	go computeFunc(*task, database)

	output := convertTaskToJsonModel(task, UriGetResult)
	w.Header().Set("Location", output.Link)       // Report created resource location for 201 code
	respondJson(w, r, output, http.StatusCreated) // Respond "201 created"
}

func getResultHandler(w http.ResponseWriter, r *http.Request, database dbapi.HashTaskDatabase) {
	if r.Method != http.MethodGet {
		http.Error(w, "405 Method not allowed, use GET", http.StatusMethodNotAllowed)
		return
	}

	idStr := path.Base(r.URL.Path)
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		respondBadRequest(w, r, err)
		return
	}
	taskId := dbapi.TaskId(id)

	log.Printf("Result requested for task %d", taskId)

	task, err := database.FindTask(taskId)
	if err == dbapi.ErrTaskNotFound {
		http.Error(w, fmt.Sprintf("404 Task %d not found", taskId), http.StatusNotFound)
		return
	}
	if err != nil {
		respondServerError(w, r, err)
		return
	}

	output := convertTaskToJsonModel(task, UriGetResult)
	respondJson(w, r, output, http.StatusOK) // Respond "200 OK"
}

func respondJson(w http.ResponseWriter, _ *http.Request, resp interface{}, httpStatus int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(httpStatus)
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		panic(err)
	}
}

func respondBadRequest(w http.ResponseWriter, _ *http.Request, err error) {
	errorText := err.Error()
	http.Error(w, "400 Bad request: "+errorText, http.StatusBadRequest)
}

func respondServerError(w http.ResponseWriter, _ *http.Request, err error) {
	errorText := err.Error()
	http.Error(w, "500 Server error: "+errorText, http.StatusInternalServerError)
}
