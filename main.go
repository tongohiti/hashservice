package main

import (
	"flag"
	"fmt"
	"hashservice/dbapi"
	"hashservice/dbinmem"
	"hashservice/dbpostgres"
	"hashservice/hasher"
	"hashservice/server"
	"log"
)

func main() {
	// Parse command line arguments
	args := parseArgs()

	// Open the database
	db := openDatabase(args)
	if db == nil {
		log.Fatalln("Failed to open database.")
		return
	}
	defer func() { _ = db.Close() }()

	// Load and run non-finished tasks
	err := runUnfinishedTasks(db)
	if err != nil {
		log.Fatalln("Failed to load unfinished tasks from database.")
		return
	}

	// Run the HTTP server
	hostPort := fmt.Sprintf(":%d", args.port)
	server.Run(hostPort, db, computeTask)
}

type args struct {
	noDatabase bool
	isPostgres bool
	dataSource string
	port       int
}

func parseArgs() args {
	noDatabase := flag.Bool("no-database", false, "Run without a database (debugging)")
	isPostgres := flag.Bool("postgres", false, "Run with PostgreSQL database")
	dataSource := flag.String("datasource", "", "SQL data source string")
	port := flag.Int("port", 8080, "Port number to listen, default is 8080")
	flag.Parse()

	return args{
		noDatabase: *noDatabase,
		isPostgres: *isPostgres,
		dataSource: *dataSource,
		port:       *port,
	}
}

func openDatabase(args args) dbapi.HashTaskDatabase {
	var (
		db  dbapi.HashTaskDatabase
		err error
	)

	switch {
	case args.noDatabase && !args.isPostgres:
		db, err = dbinmem.NewInMemoryDatabase()
		if err != nil {
			log.Fatalf("Failed to initialize in-memory db: %v", err)
			return nil
		}

	case !args.noDatabase && args.isPostgres && args.dataSource != "":
		db, err = dbpostgres.Connect(args.dataSource)
		if err != nil {
			log.Fatalf("Failed to connect to PostgreSQL database `%s`\n%v", args.dataSource, err)
			return nil
		}

	default:
		log.Fatalln("No database defined. Use either --no-database or --postgres with --datasource <connection-string>")
		return nil
	}

	return db
}

func runUnfinishedTasks(db dbapi.HashTaskDatabase) error {
	tasks, err := db.LoadUnfinishedTasks()
	if err != nil {
		return err
	}
	for _, task := range tasks {
		log.Printf("Loaded unfinished task %d: compute %d rounds of hash for text `%s`", task.Id, task.Rounds, task.Text)
		go computeTask(task, db)
	}
	return nil
}

func computeTask(task dbapi.HashTask, db dbapi.HashTaskDatabase) {
	hash, err := hasher.HashText(task.Text, task.Rounds)
	if err != nil {
		log.Printf("[task %d] Failed to compute %d rounds of hash for text `%s`: %v", task.Id, task.Rounds, task.Text, err)
		return
	}

	log.Printf("[task %d] Finished computing %d rounds of hash for text `%s`: %s", task.Id, task.Rounds, task.Text, hash)

	err = db.FinishTask(task.Id, hash)
	if err != nil {
		log.Printf("Failed to store result for task %d: %v", task.Id, err)
		return
	}
}
