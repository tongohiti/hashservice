package dbinmem

import (
	"github.com/stretchr/testify/assert"
	"hashservice/dbapi"
	"testing"
)

func TestInMemoryDatabaseInterface(t *testing.T) {
	var (
		db   dbapi.HashTaskDatabase
		task *dbapi.HashTask
		err  error
	)

	// Create database, verify it not fails
	db, err = NewInMemoryDatabase()
	assert.NotNil(t, db)
	assert.NoError(t, err)

	// Verify database is empty
	task, err = db.FindTask(dbapi.TaskId(1))
	assert.Nil(t, task)
	assert.EqualError(t, err, "task not found")

	err = db.FinishTask(dbapi.TaskId(1), "dummy")
	assert.EqualError(t, err, "task not found")

	// Create task, verify status
	task, err = db.CreateTask("the answer is 42", 3)
	assert.NotNil(t, task)
	assert.NoError(t, err)
	assert.Equal(t, task.Id, dbapi.TaskId(1))
	assert.Equal(t, task.Text, "the answer is 42")
	assert.Equal(t, task.Rounds, 3)
	assert.Equal(t, task.Status, dbapi.InProgress)
	assert.Nil(t, task.Hash)

	// Find in-progress task, verify that all fields are persisted properly
	task, err = db.FindTask(task.Id)
	assert.NotNil(t, task)
	assert.NoError(t, err)
	assert.Equal(t, task.Id, dbapi.TaskId(1))
	assert.Equal(t, task.Text, "the answer is 42")
	assert.Equal(t, task.Rounds, 3)
	assert.Equal(t, task.Status, dbapi.InProgress)
	assert.Nil(t, task.Hash)

	// Finish task
	err = db.FinishTask(task.Id, "424242")
	assert.NoError(t, err)

	// Find finished task, verify that all fields are persisted properly
	task, err = db.FindTask(task.Id)
	assert.NotNil(t, task)
	assert.NoError(t, err)
	assert.Equal(t, task.Id, dbapi.TaskId(1))
	assert.Equal(t, task.Text, "the answer is 42")
	assert.Equal(t, task.Rounds, 3)
	assert.Equal(t, task.Status, dbapi.Finished)
	assert.NotNil(t, task.Hash)
	assert.Equal(t, *task.Hash, "424242")

	// Create another task, verify it has different ID
	task, err = db.CreateTask("another task", 42)
	assert.NotNil(t, task)
	assert.NoError(t, err)
	assert.NotEqual(t, task.Id, dbapi.TaskId(1))

	// Close database, verify no errors here
	err = db.Close()
	assert.NoError(t, err)
}
