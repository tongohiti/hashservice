package dbinmem

import "hashservice/dbapi"

type InMemoryDatabase struct {
	tasks map[dbapi.TaskId]dbapi.HashTask
	curId dbapi.TaskId
}

func NewInMemoryDatabase() (dbapi.HashTaskDatabase, error) {
	db := InMemoryDatabase{tasks: make(map[dbapi.TaskId]dbapi.HashTask), curId: 0}
	return &db, nil
}

func (db *InMemoryDatabase) Close() error {
	return nil
}

func (db *InMemoryDatabase) CreateTask(text string, rounds int) (*dbapi.HashTask, error) {
	db.curId += 1
	id := db.curId

	task := dbapi.HashTask{
		Id:     id,
		Text:   text,
		Rounds: rounds,
		Status: dbapi.InProgress,
		Hash:   nil,
	}

	db.tasks[id] = task

	return &task, nil
}

func (db *InMemoryDatabase) FinishTask(id dbapi.TaskId, hash string) error {
	task, found := db.tasks[id]
	if !found {
		return dbapi.ErrTaskNotFound
	}

	task.Hash = &hash
	task.Status = dbapi.Finished

	db.tasks[id] = task

	return nil
}

func (db *InMemoryDatabase) FindTask(id dbapi.TaskId) (*dbapi.HashTask, error) {
	task, found := db.tasks[id]
	if !found {
		return nil, dbapi.ErrTaskNotFound
	}
	return &task, nil
}

func (db *InMemoryDatabase) LoadUnfinishedTasks() ([]dbapi.HashTask, error) {
	return nil, nil
}
